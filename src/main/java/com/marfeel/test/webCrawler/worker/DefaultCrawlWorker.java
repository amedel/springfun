package com.marfeel.test.webCrawler.worker;

import java.io.IOException;
import java.time.Instant;

import javax.net.ssl.SSLHandshakeException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.marfeel.test.webCrawler.evaluator.MarfeelizableEvaluator;
import com.marfeel.test.webCrawler.mongodb.VisitedSite;
import com.marfeel.test.webCrawler.mongodb.repository.VisitedSiteRepository;

public class DefaultCrawlWorker implements CrawlWorker  {

	private String url;
	private VisitedSiteRepository visitedSiteRepository;
	private MarfeelizableEvaluator marfeelizableEvaluator;

	private Logger logger = LoggerFactory.getLogger(CrawlWorker.class);

	public DefaultCrawlWorker(String url, VisitedSiteRepository visitedSiteRepository,
			MarfeelizableEvaluator marfeelizableEvaluator) {
		this.url = url;
		this.visitedSiteRepository = visitedSiteRepository;
		this.marfeelizableEvaluator = marfeelizableEvaluator;
	}

	@Override
	public Void call() throws Exception {
		visitedSiteRepository.save(new VisitedSite(null,url, isUrlMarfeelizable(), Instant.now()));
		return null;
	}
	
	private boolean isUrlMarfeelizable() {
		Document document = null;
		try {
			document = Jsoup.connect("http://" + url).get();
		} catch (SSLHandshakeException se) {
			try {
				logger.error("SSL Error consulting the site with url: http://" + url);
				document = Jsoup.connect("https://" + url).get();
			} catch (IOException e) {
				logger.error("Error consulting the site with url: https://" + url, e);
			}
		} catch (IOException e) {
			logger.error("Error consulting the site with url: http://" + url, e);
		}
		return document!=null?marfeelizableEvaluator.isMarfeelizableSite(document):false;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
