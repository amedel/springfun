package com.marfeel.test.webCrawler.service;

import java.util.List;

import com.marfeel.test.webCrawler.request.body.Site;

public interface CrawlerService {
	
	public void crawlSites(List<Site> sites);
	
}
