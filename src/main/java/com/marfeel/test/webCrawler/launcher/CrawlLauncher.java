package com.marfeel.test.webCrawler.launcher;

import com.marfeel.test.webCrawler.request.body.Site;

public interface CrawlLauncher {
	
	public void launchThread(Site site);
}
