package com.marfeel.test.webCrawler.launcher;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.springframework.beans.factory.annotation.Autowired;

import com.marfeel.test.webCrawler.request.body.Site;
import com.marfeel.test.webCrawler.worker.factory.CrawlWorkerFactory;

public class DefaultCrawlLauncher implements CrawlLauncher {

	private static final Executor executor = Executors.newCachedThreadPool();

	@Autowired
	CrawlWorkerFactory crawlWorkerFactory;

	@Override
	public synchronized void launchThread(Site site) {
		FutureTask<Void> futureCrawlWorker =  new FutureTask<>(crawlWorkerFactory.create(site.getUrl()));
		executor.execute(futureCrawlWorker);
	}

}
