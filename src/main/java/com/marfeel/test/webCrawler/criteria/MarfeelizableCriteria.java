package com.marfeel.test.webCrawler.criteria;

import org.jsoup.nodes.Document;

public interface MarfeelizableCriteria {

	boolean isMarfeelizable(Document document);
}
