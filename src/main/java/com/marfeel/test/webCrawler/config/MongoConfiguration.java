package com.marfeel.test.webCrawler.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
@EnableMongoRepositories(basePackages = "com.marfeel.test")
public class MongoConfiguration {

	@Value("${webcrawler.marfeelizable.mongodb.url}")
	private String mongoConectionUrl;

	@Bean
	public MongoClient mongo() {
		return MongoClients.create(new ConnectionString(mongoConectionUrl));
	}

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongo(), "webcrawler");
	}

}
