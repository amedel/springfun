package com.marfeel.test.webCrawler.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.marfeel.test.webCrawler.criteria.TitleMarfeelizableCriteria;

@RunWith(MockitoJUnitRunner.class)
public class TitleMarfeelizableCriteriaTest {
	
	@Mock
	Document document;

	TitleMarfeelizableCriteria titleMarfeelizableCriteria;
	
	@Before
	public void setup() {
		titleMarfeelizableCriteria = new TitleMarfeelizableCriteria(Arrays.asList("news","noticias"));
	}
	
	@Test
	public void should_return_is_marfeelizable() {
		
		Mockito.when(document.title()).thenReturn("the global news");
		assertTrue(titleMarfeelizableCriteria.isMarfeelizable(document));
		
	}
	
	@Test
	public void should_return_is_not_marfeelizable() {
		
		Mockito.when(document.title()).thenReturn("the tests");
		assertFalse(titleMarfeelizableCriteria.isMarfeelizable(document));
		
	}
}
